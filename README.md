# How to build

```
sudo singularity build xpra-visualization.simg xpra-singularity.def
```
# How to get latest container from CI
```
wget -O xpra-visualization.simg https://gitlab.com/api/v4/projects/OlegZharkov%2Fvisualization-gpu-xpra-singularity-container/jobs/artifacts/master/raw/xpra-visualization.simg?job=build
```
or sandbox:
```
wget -O singularity-xpra-img.zip https://gitlab.com/OlegZharkov/visualization-gpu-xpra-singularity-container/-/jobs/artifacts/master/download?job=build-sandbox
```
# How to get xpra singularity container with installed drivers
```
wget -O xpra-visualization.simg https://gitlab.com/api/v4/projects/OlegZharkov%2Fvisualization-gpu-xpra-singularity-container/jobs/artifacts/master/raw/xpra-visualization.simg?job=build-with-drivers
```
# How to get latest Xorg centOS container with drivers from CI

```
wget -O centos-xorg.simg https://gitlab.com/api/v4/projects/OlegZharkov%2Fvisualization-gpu-xpra-singularity-container/jobs/artifacts/master/raw/centos-xorg.simg?job=build-xorg
```
or sandbox:

```
wget -O centos-xorg-sandbox.zip https://gitlab.com/OlegZharkov/visualization-gpu-xpra-singularity-container/-/jobs/artifacts/master/download?job=build-xorg-sandbox
```
