#!/bin/bash
getent passwd xorg-runner > /dev/null
if [ $? -ne 0 ]; then
 adduser --shell /usr/sbin/nologin xorg-runner
fi
