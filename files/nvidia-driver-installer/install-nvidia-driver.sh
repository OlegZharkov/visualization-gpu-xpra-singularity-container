#!/bin/sh

# Download drivers from nvidia website
wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/cuda-repo-ubuntu1804_10.1.105-1_amd64.deb
dpkg -i cuda-repo-ubuntu1804_10.1.105-1_amd64.deb
apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub

# Nvidia driver instalation, designed for containers
NVIDIA_DRIVER_VERSION="418.39-0ubuntu1"
echo $NVIDIA_DRIVER_VERSION
apt-get update && apt-get install -y xserver-xorg-core \
libnvidia-fbc1-418=$NVIDIA_DRIVER_VERSION \
libnvidia-ifr1-418=$NVIDIA_DRIVER_VERSION \
libnvidia-cfg1-418=$NVIDIA_DRIVER_VERSION \
xserver-xorg-video-nvidia-418=$NVIDIA_DRIVER_VERSION \
nvidia-utils-418=$NVIDIA_DRIVER_VERSION \
libnvidia-encode-418=$NVIDIA_DRIVER_VERSION \
libnvidia-decode-418=$NVIDIA_DRIVER_VERSION \
nvidia-compute-utils-418=$NVIDIA_DRIVER_VERSION \
libnvidia-compute-418=$NVIDIA_DRIVER_VERSION \
nvidia-kernel-source-418=$NVIDIA_DRIVER_VERSION \
nvidia-dkms-418=$NVIDIA_DRIVER_VERSION \
libnvidia-gl-418=$NVIDIA_DRIVER_VERSION \
nvidia-driver-418=$NVIDIA_DRIVER_VERSION
