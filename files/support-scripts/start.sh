#!/bin/bash
#
if [ $# -eq 0 ]
  then
    echo "No port supplied! Using random free port!"
	PORT="$(python -c 'import socket; s=socket.socket(); s.bind(("", 0)); print(s.getsockname()[1]); s.close()')"
  else
    PORT=$1
fi

xpra start :$PORT --bind-tcp=0.0.0.0:$PORT --html=on --systemd-run=no --dbus-launch=no --webcam=no --clipboard=no --pulseaudio=yes --speaker=on --exec-wrapper=vglrun
export DISPLAY=$PORT